package com.example.julian.testapp;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;

public class BleHandle extends Activity {
    private BluetoothManager mBluetoothManager ;
    private BluetoothAdapter mBluetoothAdapter;
    public BluetoothLeScanner btScanner;
    Context context;
    private List<ScanFilter> mScanFilter;
    private ScanSettings mScanSettings;
    private Integer ret;
    private DeviceList mDeviceList = DeviceList.getInstance();
    private Integer test = 0;
    private BluetoothGatt mGATT = null;
    private static BleHandle ourInstance = null;
    private UUID mEnvironmentServiceUUID = UUID.fromString("00000002-0000-0000-FDFD-FDFDFDFDFDFD");
    private UUID mTemperatureCharacteristicUUID = UUID.fromString("00002a1c-0000-1000-8000-00805f9b34fb");
    private UUID mHumidityCharacteristicUUID = UUID.fromString("00002a6f-0000-1000-8000-00805f9b34fb");
    private UUID mFanServiceUUID = UUID.fromString("00000001-0000-0000-FDFD-FDFDFDFDFDFD");
    private BluetoothGattDescriptor mTemperatureDescriptor = null;
    private BluetoothGattCharacteristic mTemperatureCharacteristic;
    private BluetoothGattCharacteristic mHumidityCharacteristic;
    private BluetoothGattCharacteristic mFanCharacteristic = null;
    private boolean mHumidityNotifyEnabled = false;
    private boolean mTemperatureNotifyEnabled = false;
    public boolean mWriteReady = true;

    public BleHandle(Context context){
        this.context = context;
        ourInstance = this;
    }

    public static BleHandle getInstance(){
        return ourInstance;
    }

    public void setmGATT(BluetoothGatt item){
        this.mGATT = item;
    }

    private ScanCallback mScanCallback = new ScanCallback(){
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            Log.d("LOG","Single Result");
            mDeviceList.add(result.getDevice());




        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d("LOG","Scanning Error");
            Log.d("LOG", "Errorcode: "+ errorCode);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            for(int i = 0; i<results.size(); i ++){
                mDeviceList.add(results.get(i).getDevice());
            }

            Log.d("LOG","Batch Results..."+ test);
            test += 1;
        }
    };


    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(gatt, txPhy, rxPhy, status);
            Log.d("LOG","phyUpdate");
        }

        @Override
        public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyRead(gatt, txPhy, rxPhy, status);
            Log.d("LOG","phyRead");
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d("LOG","ConnectionStateChanged");
            if(status != GATT_SUCCESS){
                Log.d("LOG", "ERROR while Connecting/Disconnecting");
            }
            if(newState == STATE_DISCONNECTED){
                Log.d("LOG","BLE Disconnected");
            }
            else{
                Log.d("LOG", "BLE Connected");
                MainActivity.getInstance().onBluetoothConnected();
                BleHandle.getInstance().setmGATT(gatt);
                BleHandle.getInstance().btScanner.stopScan(mScanCallback);
            }

        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            Log.d("LOG","Services Discovered");
            if(status != GATT_SUCCESS){
                Log.d("LOG", "ERROR Reading Services");
            }
            else{
                List<BluetoothGattService> mBluetoothGattServices = gatt.getServices();
                List<BluetoothGattCharacteristic> mBluetoothGattCharacteristics;
                UUID temp;
                UUID chartemp;
                for(int i = 0; i < mBluetoothGattServices.size(); i++){
                    temp = mBluetoothGattServices.get(i).getUuid();
                    Log.d("LOG" , ""+temp);
                    if(temp.equals(mEnvironmentServiceUUID)){
                        Log.d("LOG", "Looking up Characteristics");

                        //Enable Notify



                        mTemperatureCharacteristic = mBluetoothGattServices.get(i).getCharacteristic(mTemperatureCharacteristicUUID);
//                        gatt.setCharacteristicNotification(mTemperatureCharacteristic, true);
//                        BluetoothGattDescriptor descriptor = mTemperatureCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
//                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//                        gatt.writeDescriptor(descriptor);



                        mHumidityCharacteristic = mBluetoothGattServices.get(i).getCharacteristic(mHumidityCharacteristicUUID);
//                        gatt.setCharacteristicNotification(mHumidityCharacteristic, true);
//                        descriptor = mHumidityCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
//                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//                        gatt.writeDescriptor(descriptor);
                        enableNotify();

                    }
                    if(temp.equals(mFanServiceUUID)){
                        //notify start Fan job
                        mFanCharacteristic = mBluetoothGattServices.get(i).getCharacteristic(UUID.fromString("10000001-0000-0000-FDFD-FDFDFDFDFDFD"));
                        Log.d("LOG", "Fan Service");
                        byte[] arr=new byte[]{(byte)0xFF,(byte)0xFF};



                    }
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d("LOG","CharacteristicRead");

        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.d("LOG","CharacteristicWrite");
            mWriteReady = true;
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d("LOG","CharacteristicChanged: " + characteristic.getUuid());

            if(characteristic.getUuid().equals(mTemperatureCharacteristicUUID)) {
                float tempValue = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_FLOAT, 1);
                ConnectedActivity.getInstance().updateTemp(tempValue);
            }

            if(characteristic.getUuid().equals(mHumidityCharacteristicUUID)){
                byte[] temp = characteristic.getValue();
                ByteBuffer bb = ByteBuffer.allocate(2);
                bb.order(ByteOrder.LITTLE_ENDIAN);
                bb.put(temp[0]);
                bb.put(temp[1]);
                short shortVal = bb.getShort(0);
                float humidity = (float) 0.01 * shortVal;
                ConnectedActivity.getInstance().updateHumidity(humidity);


            }


        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            Log.d("LOG","DescriptorRead");
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            Log.d("LOG","DescriptorWrite");
            if(descriptor.getCharacteristic().getUuid().equals(mHumidityCharacteristicUUID)){
                mHumidityNotifyEnabled = true;
            }
            if(descriptor.getCharacteristic().getUuid().equals(mTemperatureCharacteristicUUID)){
                mTemperatureNotifyEnabled = true;
            }
            enableNotify();

        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            Log.d("LOG","ReliableWriteComplete");
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Log.d("LOG","ReadRemoteRSSI");
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            Log.d("LOG","MtuChanged");
        }
    };


    public void enableNotify(){
        if(!mTemperatureNotifyEnabled){
            mGATT.setCharacteristicNotification(mTemperatureCharacteristic, true);
            BluetoothGattDescriptor descriptor = mTemperatureCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mGATT.writeDescriptor(descriptor);
        }
        else {
            if (!mHumidityNotifyEnabled) {
                mGATT.setCharacteristicNotification(mHumidityCharacteristic, true);
                BluetoothGattDescriptor descriptor = mHumidityCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                mGATT.writeDescriptor(descriptor);
            }
        }
    }



    public void triggerScan(boolean startScanning){
        if(startScanning)
        {
            //Start Scanning
            //btScanner.startScan(mScanFilter,mScanSettings,mScanCallback);
            btScanner.startScan(mScanCallback);
            Log.d("LOG","Scanning Started");
        }
        else
        {
            //Stop Scanning
            btScanner.stopScan(mScanCallback);
            Log.d("LOG", "Scanning Stopped");
        }
    }

    public void initialize(){
        mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        btScanner = mBluetoothAdapter.getBluetoothLeScanner();



        mScanFilter = new ArrayList<>();
        ScanSettings.Builder builderScanSettings = new ScanSettings.Builder();
        builderScanSettings.setReportDelay(0);
        mScanSettings = builderScanSettings.build();
        Log.d("LOG","Initialize Bluetooth");




    }


    public void connect(BluetoothDevice item){
        Log.d("LOG","Start Connecting");
        item.connectGatt(context,true,mGattCallback);

    }

    public void discoverServices(){
        this.mGATT.discoverServices();

    }


    public void setFanSpeed(short value){
        if(mFanCharacteristic != null && mWriteReady){
            mWriteReady = false;


            byte[] arr=new byte[]{(byte)(value&0xFF),(byte)(value>>>8)};

            Log.d("LOG","Updating Speed");
            mFanCharacteristic.setValue(arr);
            mGATT.writeCharacteristic(mFanCharacteristic);


        }

    }


}
